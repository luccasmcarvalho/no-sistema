import 'package:flutter/material.dart';
import 'package:no_newspaper_oficial/edicaoaberta.dart';
import 'package:no_newspaper_oficial/admpage.dart';

class RegisterUser extends StatefulWidget {
  @override
  _RegisterUserState createState() => _RegisterUserState();
}

class _RegisterUserState extends State<RegisterUser> {
  final GlobalKey<FormState> _formKeyValue = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.height * 2,
            height: 80,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0xFF171E76),
                    Color(0xFF1C1452),
                  ],
                ),
                borderRadius: BorderRadius.only(topLeft: Radius.circular(120))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Registrar",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                    fontFamily: 'Roboto',
                  ),
                ),
                Text(
                  "usuário",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                    fontFamily: 'Roboto',
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20.0),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(width: 40.0),
              Icon(Icons.assignment_ind, size: 120.0, color: Color(0xff1C1452)),
              SizedBox(height: 20.0),
              Form(
                key: _formKeyValue,
                autovalidate: true,
                child: new Column(
                  //padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  children: <Widget>[
                    SizedBox(height: 20.0),
                    new TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {}
                        return null;
                      },
                      decoration: const InputDecoration(
                        icon: const Icon(
                          Icons.account_circle,
                          color: Colors.amber,
                          size: 50,
                        ),
                        labelText: 'Nome',
                        hintText: 'Informe seu nome',
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
